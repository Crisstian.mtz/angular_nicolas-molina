import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { ProductsService } from './../../core/services/products.service';
//import { ProductsService } from './../../shared/services/products.service';
import { Product } from './../../shared/models/components/product.interface';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {
  product: Product;

  constructor(
    private route: ActivatedRoute,
    private productsService: ProductsService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.fetchProduct(id);
      //this.product = this.productsService.getProduct(id);
    });
  }

  fetchProduct(id: string) {
    this.productsService.getProduct(id).subscribe(product => {
      this.product = product;
    });
  }

  createProduct() {
    const newProduct: Product = {
      id: '222',
      title: 'nuevo',
      image: '../../../assets/images/banner-1.jpg',
      price: 50,
      description: 'Nuevo de paquete, 0km',
    };
    this.productsService.createProduct(newProduct).subscribe(product => {
      console.log(product);
    });
  }

  updateProduct() {
    const updateProduct: Partial<Product> = {
      id: '13',
      title: 'nuevo editado',
      image: '../../../assets/images/banner-1.jpg',
      price: 5000,
      description: 'Nuevo editado',
    };
    this.productsService.updateProduct('13', updateProduct).subscribe(product => {
      console.log(product);
    });
  }

  deleteProduct() {
    this.productsService.deleteProduct('13').subscribe(rta => {
      console.log(rta);
    });
  }
}
