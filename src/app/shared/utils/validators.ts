import { AbstractControl } from '@angular/forms';

export class MyValidator {
  static isPricedValid(control: AbstractControl) {
    const value = control.value;
    if (value > 10000) {
      return { price_invalid: true };
    } else {
      return null;
    }
  }
}
