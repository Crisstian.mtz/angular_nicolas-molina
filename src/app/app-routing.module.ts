import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { ContactComponent } from './pages/contact/contact.component';
import { DemoComponent } from './pages/demo/demo.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductDetailComponent } from './pages/product-detail/product-detail.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { LayoutComponent } from './shared/components/layout/layout.component';
import { TopSecretComponent } from './pages/top-secret/top-secret.component';

import { AdminGuard } from './shared/guards/admin.guard';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', redirectTo: '/home', pathMatch: 'full' },
      { path: 'contact', component: ContactComponent },
      {
        path: 'home',
        loadChildren: () =>
          import('./pages/home/home.module').then(
            (m) => m.HomeModule
          ) /* component: HomeComponent */,
      },
      { path: 'products', component: ProductsComponent },
      { path: 'products/:id', component: ProductDetailComponent },
      {
        path: 'secret-page',
        canActivate: [AdminGuard],
        component: TopSecretComponent,
      },
      {
        path: 'order',
        loadChildren: () =>
          import('./pages/order/order.module').then(
            (m) => m.OrderModule),
      },
      { path: 'demo', component: DemoComponent },

    ],
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./pages/admin/admin.module').then(
        (m) => m.AdminModule
      ) /* component: HomeComponent */,
  },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
